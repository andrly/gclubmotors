<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/*-----------------------------------------------------------------------------------*/
/* Start Functions - Please refrain from editing this section
/*-----------------------------------------------------------------------------------*/
include( get_template_directory() . '/inc/constants.php');
include( get_template_directory() . '/inc/init.php');

//EXCERPT LENGTH FILTER
function ivy_excerpt_length( $length ) {
    return 50;
}
add_filter( 'excerpt_length', 'ivy_excerpt_length', 999 );

//EXCERPT MORE FILTER
function ivy_excerpt_more( $more ) {
    return ' ';
}
add_filter( 'excerpt_more', 'ivy_excerpt_more' );

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
    echo '<style>
        #menu-posts,
        #menu-comments,
        #toplevel_page_wpcf7,
        #menu-appearance,
        #menu-plugins,
        #menu-users,
        #toplevel_page_edit-post_type-acf-field-group,
        
        .page-title-action,
        #menu-pages .wp-submenu
        {
            display: none;
        }        
  </style>';
}

/*-----------------------------------------------------------------------------------*/
/* /End
/*-----------------------------------------------------------------------------------*/
