<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/*-----------------------------------------------------------------------------------*/
/* Load the files for theme, with support for overriding the widget via a child theme.
/*-----------------------------------------------------------------------------------*/
include( get_template_directory() . '/inc/theme-essentials.php');  // Theme Essentials
include( get_template_directory() . '/inc/aq_resizer.php');         // aq_resizer

if ( ! isset( $content_width ) )
	$content_width = 1140;


/**
 * Basic Theme Setup
 */

if ( ! function_exists( 'theme_setup' ) ) {

	function theme_setup() {

		// Load the translations
		load_theme_textdomain( 'gclubmotors', get_template_directory() . '/lang' );

		// Add default posts and comments RSS feed links to head
		add_theme_support( 'automatic-feed-links' );

		add_theme_support( "custom-header" );

		// Add admin editor style.
		add_editor_style( 'inc/editor-style.css' );

		// Add custom background support.
		add_theme_support( 'custom-background', array( 'default-color' => 'ffffff' ) );

		// Enable Post Thumbnails ( Featured Image )
		add_theme_support( 'post-thumbnails' );

		// Title tag support
		add_theme_support( 'title-tag' );

		// Register Navigation Menus
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu', 'ivy' ),
		) );

		// Enable support for HTML5 markup.
		add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form' ) );

	}

} add_action( 'after_setup_theme', 'theme_setup' );

