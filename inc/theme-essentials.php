<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/*-----------------------------------------------------------------------------------*/
/* Theme essentials! */
/*-----------------------------------------------------------------------------------*/

function get_font_url() {
	$font_url = '';
	/*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
	if ( 'off' !== _x( 'on', 'Google font: on or off', 'ivy' ) ) {
        $fonts='Poppins:300,400,500';

		$font_url = add_query_arg( 'family', urldecode($fonts) , "//fonts.googleapis.com/css" );
	}
	return $font_url;
}

if ( ! function_exists( 'load_scripts' ) ) {
	function load_scripts() {
        // Scripts

        // all
        wp_enqueue_script( 'lightgallery', THEME_URI . 'assets/js/lightgallery.min.js', array('jquery'));
        wp_enqueue_script( 'wow', THEME_URI . 'assets/js/wow.min.js', array('jquery'));
        wp_enqueue_script( 'global', THEME_URI . 'assets/js/global.js', array('jquery'), null, true );
        //only for map
        wp_enqueue_script( 'google_maps', 'http://maps.googleapis.com/maps/api/js?key=AIzaSyCOJxqBTpjRXdgM8UM0UXuLCmK_AKF9NPs&language=en', array('jquery'));
        wp_enqueue_script( 'map-customize', THEME_URI . 'assets/js/map.js', array('jquery'));

		// Styles
        wp_enqueue_style( 'google_fonts', get_font_url() , '', null );

        wp_enqueue_style( 'bootstrap-grid-min', THEME_URI . 'assets/css/bootstrap-grid.min.css' );
        wp_enqueue_style( 'lightgallery', THEME_URI . 'assets/css/lightgallery.min.css' );
        wp_enqueue_style( 'animate', THEME_URI . 'assets/css/animate.min.css' );
        wp_enqueue_style( 'custom-style', THEME_URI . 'assets/css/style.css' );
        wp_enqueue_style( 'style', THEME_URI . 'style.css' );

	}
	add_action( 'wp_enqueue_scripts', 'load_scripts' );
}
