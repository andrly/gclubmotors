var _functions = {};

jQuery(function($) {
	"use strict";

	/*================*/
	/* 01 - VARIABLES */
	/*================*/
	var swipers = [], winW, winH, headerH, winScr, footerTop, _isresponsive,
		_ismobile = navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i);

	/*========================*/
	/* 02 - page calculations */
	/*========================*/
	_functions.pageCalculations = function () {
		winW = $(window).width();
		winH = $(window).height();
	};

	/*=================================*/
	/* 03 - function on document ready */
	/*=================================*/
	if (_ismobile) $('body').addClass('mobile');
	_functions.pageCalculations();

	/*============================*/
	/* 04 - function on page load */
	/*============================*/
	$(window).load(function () {
		$('body').addClass('loaded');
		window.setTimeout(() => {
			$('#loader-wrapper').fadeOut();
			$('.bg-banner').addClass('active');
		}, 400);
	});

	/*==============================*/
	/* 05 - function on page resize */
	/*==============================*/
	_functions.resizeCall = function () {
		_functions.pageCalculations();
	};
	if (!_ismobile) {
		$(window).resize(function () {
			_functions.resizeCall();
		});
	} else {
		window.addEventListener("orientationchange", function () {
			_functions.resizeCall();
		}, false);
	}

	/*==============================*/
	/* 06 - function on page scroll */
	/*==============================*/
	$(window).scroll(function () {
		_functions.scrollCall();
	});

	_functions.scrollCall = function () {
		winScr = $(window).scrollTop();
		if ($(window).scrollTop() >= 10) {
			$('header').addClass('active');
		} else {
			$('header').removeClass('active');
		}
	};


	/*==============================*/
	/* 08 - buttons, clicks, hovers */
	/*==============================*/

	//open and close popup
	$('.open-popup').on('click', function () {
      $('.popup-wrapper.first').addClass('active');
      $('html').addClass('overflow');
    });

    $('.close-btn, .okey_btn').on('click', function () {
      $(this).closest('.popup-wrapper.first').removeClass('active');
            $('html').removeClass('overflow');
        });

	$('.close-btn, .okey_btn').on('click', function () {
		$(this).closest('.popup-wrapper.success').removeClass('active');
		$('html').removeClass('overflow');
	});


	$(".gallery-wrapp").lightGallery();

	var wow = new WOW(
		{
			boxClass: 'wow',
			animateClass: 'animated',
			offset: 0,
			mobile: true,
			live: true,
			scrollContainer: null,
			resetAnimation: true,
		}
	);
	wow.init();


	$("header ul a").click(function () {
		$("html, body").animate({
			scrollTop: $($(this).attr("href")).offset().top - 100
		}, {
			duration: 500,
			easing: "swing"
		});
		return false;
	});

	$(".learn-more a").click(function () {
		$([document.documentElement, document.body]).animate({
			scrollTop: $("#project").offset().top
		}, 2000);
	});

	$('header ul li a').on('click', function () {
		$('.popup-wrapper').removeClass('active');
		$('html').removeClass('overflow');
	});

    document.addEventListener( 'wpcf7submit', function( event ) {
        if ( '70' == event.detail.contactFormId && event.detail.apiResponse.status == "mail_sent" ) {
            $('.popup-wrapper.first').removeClass('active');
            $('.popup-wrapper.success').addClass('active');
			//$('.popup-wrapper.active .content h4').html(event.detail.apiResponse.message);
			setTimeout(function() {
				//$('.popup-wrapper.success').removeClass('active');
				//$('html').addClass('overflow');
                //$('.popup-wrapper').removeClass('active');
                //$('html').removeClass('overflow');
                //$('.popup-wrapper .content h4').html('Write us what you <br> are interested in');
                //$('.popup-wrapper .form').show();
			}, 3000);
        }
    }, false );
});
